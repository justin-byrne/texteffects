<h1>textEffects</h1>

<p>
This little application fundamentally takes advantage of the Canvas
Text API, which allows developers to render text directly inside any
HTML5 document. Prior to the inception of the Canvas Text API, these
effects where difficult (if not impossible) to achieve in either CSS
and/or JavaScript, whereas now they can be easily executed (on the
fly) with only a bit of code. Please, feel free to play around with
its user interface, and see what kind of results you can get.
Furthermore, if you like your results, you can easily export your
image as a string of image data (MIME type image/png) or a “.png”
file; using the toDataURL() method.
</p>

<h1>Live Demo</h1>

<p>
<a href="http://byrne-systems.com/texteffects/">http://byrne-systems.com/texteffects/</a>
</p>
